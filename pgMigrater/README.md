## Migration
* [SQLine - Oracle to PostgreSQL Migration](http://www.sqlines.com/oracle-to-postgresql) - C++， Include migrator and PLSQL Converter
* [mysql-postgresql-converter](https://github.com/lanyrd/mysql-postgresql-converter) - Lanyrd's MySQL to PostgreSQL conversion script.
* [ora2pg](http://ora2pg.darold.net) - [Open source and free] Perl module to export an Oracle database schema to a PostgreSQL compatible schema.
* [pgloader](https://github.com/liuyuanyuan/fantastic-postgres/blob/master/pgMigrater/pgloader/pgloader_intro.md)
* [Ispirer Migration](http://wiki.ispirer.com/sqlways)
* [Comparison of database tools](https://en.wikipedia.org/wiki/Comparison_of_database_tools)
* [PG WIKI - Oracle_to_Postgres_Conversion](https://wiki.postgresql.org/wiki/Oracle_to_Postgres_Conversion)


## Oracle Compatible
* [Orafce](https://github.com/orafce/orafce) Functions and operators that emulate a subset of functions and packages from the Oracle RDBMS.


## ETL
* [Data Integration (or Kettle)](https://community.hitachivantara.com/docs/DOC-1009855)


## Sync
* [HVR](https://www.hvr-software.com)


## Migration Knowledge Base
* [TangCheng oracle2pg Migrate Blog](http://osdbablog.sinaapp.com/528.html)
* [Oracle vs PG Diff](https://my.oschina.net/liyuj/blog/539303)
* [Oracle vs PG System Function Diff](https://yq.aliyun.com/users/1994466589078092?spm=a2c4e.11153940.blogcont59422.2.2130505bZglu5d)

