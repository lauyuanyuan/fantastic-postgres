# PG Database GUI IDE Tool

## [Comparison_of_database_tools](https://en.wikipedia.org/wiki/Comparison_of_database_tools)

## Desktop Admin
* [DataGrid](https://www.jetbrains.com/datagrip/?fromMenu)
  [By JetBrains] Cross-Platform IDE for multi Databases & SQL 


* [Navicate for PostgreSQL](https://www.navicat.com/en/products/navicat-for-postgresql)
  [By CyberTech] Cross-Platform IED for multi Databases


* [DBeaver]()
  [Open source and free] Cross-Platform IED for multi databases

* [pgAdmin3]()
  [Open source and free] Cross-Platform IED for PG

* [pgAdmin4 based on pgAdmin3]()
 [Open source and free] Cross-Platform IED for PG for both desktop and web GUI.
 

* [DBForge Studio for PG](https://www.devart.com/dbforge/postgresql/studio/)
* [DBForge Data Compara](https://www.devart.com/dbforge/postgresql/datacompare/)


## Web Admin
* [pgAdmin4 based on pgAdmin3](https://github.com/postgres/pgadmin4)
  [Open source and free] python web GUI.

* [phpPgAdmin](https://github.com/xzilla/phppgadmin)
  [Open source and free] php web IDE 

* [Adminer](https://github.com/vrana/adminer)
  [Open source and free] php web IDE



 
